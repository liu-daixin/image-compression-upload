import axios from "axios";
export function request(config) {
    // 1.创建axios的实例
    const instance = axios.create({
            baseURL: '/api',
            timeout: 50000
        })
        // 2.发送真正的网络请求
    return instance(config) //本身返回的就是一个Promise
}