package com.example.imgservice.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //客户端访问地址前缀
        registry.addResourceHandler("/static/**")
                .addResourceLocations("file:"+ MyContent.PRO_PATH+"/myCollect/");
    }
}
