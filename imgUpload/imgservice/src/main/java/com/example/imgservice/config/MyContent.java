package com.example.imgservice.config;


public class MyContent {
    //日期格式化模板字符串
    public static final String DATE_FORMAT_STR="yyyy-MM-dd HH:mm:ss";
    //系统文件路径分隔符
    public static final String FILE_SER = System.getProperty("file.separator");
    //项目路径
    public static final String PRO_PATH = System.getProperty("user.dir");
    //文件存储目录名
    public static final String UPLOAD_DIR="upload";
    //文件上传存储路径
    public static final String UPLOAD_PATH=PRO_PATH+FILE_SER+UPLOAD_DIR;
    //随机路径取值基础字符串(16进制)
    public static final String HEX_STR="0123456789ABCDEF";
    //JWT设置过期时间
    public static long time=1000*60*60*24;
    //JWT签名
    public static String signature="user";
}
