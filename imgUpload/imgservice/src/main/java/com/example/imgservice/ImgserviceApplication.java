package com.example.imgservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImgserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImgserviceApplication.class, args);
    }

}
